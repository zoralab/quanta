module gitlab.com/zoralab/quanta

go 1.15

replace gitlab.com/zoralab/quanta => ./

require (
	gitlab.com/zoralab/godocgen v0.0.2 // indirect
	golang.org/x/perf v0.0.0-20200918155509-d949658356f9 // indirect
	golang.org/x/tools v0.0.0-20200915201639-f4cefd1cb5ba // indirect
)
