# Quanta

![Quanta Banner](.sites/static/img/logo/quanta-4096x2048.png)

Quanta is a central control web interface for controlling a compute module over
the network. It handles all services via a centralized web interface for
headless interaction.

Visit its [Official Website](https://zoralab.gitlab.io/quanta) to get its
latest specifications and informations.
