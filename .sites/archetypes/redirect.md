{{- $titleWords := replace (replace .Name "-" " ") "_" " " -}}
{{- $keywords := split $titleWords " " -}}
{{- $title := strings.Title $titleWords -}}

+++
date = "{{ .Date }}"
title = "{{- $title -}}"
description = """
{{ $title }} post description is here.
It will be shown in Google Search bar.
"""
keywords = {{ replace (printf "%+q" $keywords) " " ", " }}
authors = ["John M. Smith", "Jane S. Smith"]
draft = true
type = ""
redirectURL="https://UPDATE.THIS.LINK"
layout = "redirect"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
	# Example: "amp-sidebar",
]


[creators.holloway]
type = "Person"
name = '"Holloway" Chew Kean Ho'


[thumbnails.0]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Quanta"

[thumbnails.1]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Quanta"


[menu.main]
parent = "redirect"
# name = "{{- $title -}}"
pre = ""
weight = 2


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
If you are not redirected, please visit:

[target title]({{< link "https://UPDATE.THIS.LINK" "this" "url-only" >}})
