// Package quanta from cmd/quanta is to build Quanta application.
//
// Quanta is a central control web interface for controlling a compute module
// over the network. It handles all services via a centralized web interface
// for headless interaction. Quanta is designed to work independently and is
// fully written in Go Programming Language.
package main

import (
	"gitlab.com/zoralab/quanta/api/cli"
)

func main() {
	cli.Print()
}
