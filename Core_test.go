package quanta

import (
	"testing"
)

func TestRun(t *testing.T) {
	t.Logf("just to test run.")
}

func BenchmarkRun(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Calculate(i)
	}
}
