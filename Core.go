package quanta

const (
	// Version is stating the current quanta version
	Version = "0.0.2"

	// Statement is a demo statement to initialize repository
	Statement = "Hello World"
)

func Calculate(n int) int {
	f1 := 0
	f2 := 1
	fnext := 0

	for i := 0; i < n; i++ {
		fnext = f1 + f2
		f1 = f2
		f2 = fnext
	}

	return f2
}
