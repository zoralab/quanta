+++
date = "2020-09-17T20:07:52+08:00"
title = "Welcome to Quanta Project"
description = """
Quanta is a central control web interface for controlling a compute module over
the network. It handles all services via a centralized web interface for
headless interaction. Quanta is designed to work independently and is fully
written in Go Programming Language.
"""
keywords = [""]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
	# Example: "amp-sidebar",
]


[creators.holloway]
type = "Person"
name = '"Holloway" Chew Kean Ho'


[thumbnails.0]
url = "/img/thumbnails/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Quanta"

[thumbnails.1]
url = "/img/thumbnails/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Quanta"


[menu.main]
parent = ""
name = "Home"
pre = "🏠"
weight = 1


[schema]
selectType = "WebPage"
+++

{{< image "Project Quanta | Welcome"
	"/img/logo/quanta-4096x2048.png"
	"4096"
	"2048"
	"false"
	"lazy"
	""
	"responsive"
	""
>}}


# {{% param "title" %}}
{{% param "description" %}}




## Why Use Quanta
Here are some of the reasons to use Quanta as your compute module's network
controller.

{{< card "quanta.cards.features" "grid" "embed" >}}




## Core Features
From time to time, Quanta identified and executed a wide varieties of features
implementations for better use in the future. Here are some of our recognizable
features implemented across Quanta versions.

| Name                                        | Status      | Starting Version |
|:--------------------------------------------|:------------|:-----------------|
| Single CI Controlled Infrastructure         | ✅Completed | `v1.0.0`         |
| Single Web Interface Control                | 📅Working   | `v0.0.1`         |
| Reverse Proxy                               | 💡Idea      | `N/A`            |

For latest information, please check out the Issues board at:
https://gitlab.com/ZORALab/quanta/issues




## Powered by Simple Tools
Quanta is powered by many simple tools integrated together in order to produce
great results. Here are some of the core technologies:

{{< svg/shieldTag "core language" "go" "#1313c3" >}}
{{< svg/shieldTag "core language" "html" "#1313c3" >}}
{{< svg/shieldTag "core language" "css" "#1313c3" >}}
{{< svg/shieldTag "core language" "javascript" "#1313c3" >}}
{{< svg/shieldTag "core language" "bash" "#1313c3" >}}
{{< svg/shieldTag "core tool" "gopher" "#03a9f4" >}}
{{< svg/shieldTag "core tool" "helix" "#ab47bc" >}}




## Quality Assured Via Continuous Integrations
To ensure that our customers are getting updates in an incremental and stable
way, Quanta uses [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/)
technologies to manage its quality assurance from release to even developer
experimentation stage.

Feel free to check out their current status here:

| Branch   | Test Status | Test Coverage |
|:---------|:------------|:--------------|
| `master` | {{< image "Master Pipeline Status"
	"https://gitlab.com/ZORALab/quanta/badges/master/pipeline.svg"
	"116"
	"20"
	"false"
	"lazy"
	""
	""
>}} | {{< image "Master Coverage Report"
	"https://gitlab.com/ZORALab/quanta/badges/master/coverage.svg"
	"120"
	"20"
	"false"
	"lazy"
	""
	""
>}} |
| `staging` | {{< image "Staging Pipeline Status"
	"https://gitlab.com/ZORALab/quanta/badges/staging/pipeline.svg"
	"116"
	"20"
	"false"
	"lazy"
	""
	""
>}} | {{< image "Staging Coverage Report"
	"https://gitlab.com/ZORALab/quanta/badges/staging/coverage.svg"
	"120"
	"20"
	"false"
	"lazy"
	""
	""
>}} |
| `next` | {{< image "Next Pipeline Status"
	"https://gitlab.com/ZORALab/quanta/badges/next/pipeline.svg"
	"116"
	"20"
	"false"
	"lazy"
	""
	""
>}} | {{< image "Next Coverage Report"
	"https://gitlab.com/ZORALab/quanta/badges/next/coverage.svg"
	"120"
	"20"
	"false"
	"lazy"
	""
	""
>}} |

To find Quanta source codes, it is available at the following repositories:

| Name          | Location                            |
|:--------------|:------------------------------------|
| GitLab.com    | https://gitlab.com/ZORALab/quanta   |



## Contributors
Quanta is not just a single person work but a culmulative of large amount of
knowledge and wisdom. Here are some of the critical contributors for making
Quanta a success.


### Maintainers
Here are the maintainers who manage Quanta's code releases and its health.

1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2020 to present


### Developers
Here are the developers who works tirelessly to enable and keeping Quanta
up-to-date.

1. (Holloway) Chew, Kean Ho (`kean.ho.chew@zoralab.com`) - 2020 to present


### Knowledge Experts
Here are the knowledge experts contributed insights and wisdom for Quanta to
be successful.

1. [Schema.org](https://schema.org/) - 2020



### Sponsorship
Here are the sponsors that provide financial supports to enable Quanta team
to continue keeping Quanta alive:

{{< card "quanta.cards.sponsors" "grid" "default" >}}


{{< note "note" "Sponsor Us" >}}
Interested to sponsor this project? Check out: [Sponsoring Section]({{< link
	"sponsoring"
	"this"
	"url-only" >}})
{{< /note >}}




## Where to Go Next?
Interested to use Quanta for your next project? Feel free to:

{{< renderHTML "html" "amp" >}}
<div class="row">
	<div class="column" style="text-align: center">
		<a class="button pinpoint"
			href="{{< link "getting-started" "this" "url-only">}}"
			style="text-align: center">
			Getting Started with Quanta
		</a>
	</div>
</div>
{{< /renderHTML >}}
