+++
date = "2020-09-17T22:30:02+08:00"
title = "Getting Started"
description = """
Let's get started with deploying Quanta in your compute module. Quanta itself
is a simplified server program built using various tools and components. That
way, Quanta can continuously improve itself in a scalable manner while making
the user experience easier.
"""
keywords = ["getting", "started", "quanta"]
draft = false
type = ""
# redirectURL=""
layout = "single"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
        # Example: "amp-sidebar",
]


[creators.ZORALab]
type = "Organization"
name = "ZORALab Team"

[creators.KeanHo]
type = "Person"
name = "Holloway Chew Kean Ho"


[thumbnails.0]
url = "getting-started-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Getting Started with Quanta"

[thumbnails.1]
url = "getting-started-1200x628.png"
width = "1200"
height = "628"
alternateText = "Getting Started with Quanta"


[menu.main]
parent = ""
name = "Getting Started"
pre = "🛫"
weight = 2


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}

As we are currently initializing the development, we will be updating this
section over time. Come back somewhere in the future!
