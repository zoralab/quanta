+++
date = "2020-09-18T15:08:14+08:00"
title = "Releases"
description = """
Releases
Post description is here. It will be shown in Google Search bar should the bot
believes it is appropriates.
"""
keywords = ["releases"]
draft = false
type = ""
# redirectURL=""
layout = "list"


[robots]
[robots.googleBot]
name = "googleBot"
content = ""


[amp]
modules = [
	# Example: "amp-sidebar",
]


[creators.holloway]
type = "Person"
name = '"Holloway" Chew Kean Ho'


[thumbnails.0]
url = "releases/default-1200x1200.png"
width = "1200"
height = "1200"
alternateText = "Quanta"

[thumbnails.1]
url = "releases/default-1200x628.png"
width = "1200"
height = "628"
alternateText = "Quanta"


[menu.main]
parent = "redirect"
name = "Release Notes"
pre = "🗃️"
weight = 2


[schema]
selectType = "WebPage"
+++

# {{% param "title" %}}
{{% param "description" %}}
